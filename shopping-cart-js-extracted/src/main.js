let tagMap = {};
let cartList = [];
let totalPrice = 0;

main();

function main() {
    setUp();
    showTag('login');
}

function setUp() {
    let rootElement = document.getElementsByTagName('root')[0];
    for (const childElement of rootElement.children) {
        tagMap[childElement.tagName.toLowerCase()] = childElement;
    }
}

function showTag(tagName) {
    for (const tag in tagMap) {
        if (tagName != tag) {
            tagMap[tag].hidden = true;
        }
    }

    // check if its cart also check if cartList has something     
    if (tagName === 'cart' && cartList.length === 0) {
        tagName = 'cartempty';
    } else if (tagName === 'cart') {
        for (const itemInCart of cartList) {
            itemInCart.id = "cart-" + itemInCart.id;
            let itemIdName = itemInCart.id;
            let button = itemInCart.getElementsByTagName("button")[0];
            let price = parseInt(itemInCart.querySelector('label.price').textContent);

            button.textContent = "Remove from cart";
            button.removeAttribute('onclick');

            button.addEventListener('click', function () {
                removeFromCart(itemIdName, price);
            });

            tagMap[tagName].append(itemInCart);
        }

        let totalPriceParentElement = document.getElementsByTagName('totalprice')[0];
        console.log(totalPriceParentElement);
        document.getElementById('total-price').textContent = totalPrice;
        totalPriceParentElement.hidden = false;
        tagMap[tagName].append(totalPriceParentElement);
    }

    tagMap[tagName].hidden = false;
}

function addToCart(itemId, price) {
    alert("Adding " + itemId.substring(1) + " to cart.");
    cartList.push(document.getElementById(itemId.substring(1)).cloneNode(true));
    totalPrice += price;
}

function removeFromCart(itemId, price) {
    console.log(price);
    document.getElementById(itemId).remove();
    alert("Removing " + itemId + " from cart.");
    cartList.pop(document.getElementById(itemId));
    totalPrice -= price;
    document.getElementById('total-price').textContent = totalPrice;
}